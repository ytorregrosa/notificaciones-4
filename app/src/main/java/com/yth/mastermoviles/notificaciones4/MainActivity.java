package com.yth.mastermoviles.notificaciones4;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.renderscript.RenderScript;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public static int NOTIFICATION_ID = 1;
    int contador = 0;
    String ns;
    NotificationManager notManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ns = Context.NOTIFICATION_SERVICE;
        notManager = (NotificationManager) getSystemService(ns);

        Button iniciar = (Button)findViewById(R.id.iniciar);
        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                contador++;
                launchNotification();
            }
        });

        Button detener = (Button)findViewById(R.id.detener);
        detener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                contador = 0;
                notManager.cancelAll();
            }
        });
    }

    private void launchNotification() {

        NotificationCompat.Builder notBuilder;

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pendIntent = PendingIntent.getActivity(
                getApplicationContext(), 0, intent, 0);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setTicker("Tareas iniciadas: "+contador)
                .setNumber(contador)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentTitle("Tareas iniciadas: "+contador)
                .setSmallIcon(android.R.drawable.sym_def_app_icon)
                .setAutoCancel(true)
                .setSound(alarmSound)
                .setVibrate(new long[] { 1000, 1000 })
                .setLights(Color.GREEN, 2000, 2000)
                .setContentIntent(pendIntent)
                .setAutoCancel(true);

        notManager.notify(NOTIFICATION_ID, notBuilder.build() );
    }
}
